require 'mkmf'

def add_define(name)
  $defs.push("-D#{name}")
end

if !have_library('pcre') or !have_header('pcre.h')
  fail <<-EOM
  Can't find libpcre or pcre.h

  EOM
end

have_func('pcre_free_study')
if RUBY_VERSION > "1.9"
  add_define "USE_ENCODING"
end
create_makefile('ruby_pcre/ruby_pcre')
