#include <ruby.h>
#include <ruby/encoding.h>
#include <pcre.h>

#ifndef PCRE_CONFIG_JIT
#define PCRE_STUDY_JIT_COMPILE 0
#endif

VALUE mPcre;
VALUE cRegex;
VALUE cMatch;
VALUE cPcreInternal;

ID sym_puts;

ID ivar_position;
ID ivar_length;
ID ivar_vector;
ID ivar_matched_str;
ID ivar_error_text;
ID ivar_error_offset;
ID ivar_error_code;
ID ivar_internal_pcre;
ID ivar_pattern;

typedef struct PCRE_INTERNAL {
  pcre* pcre;
  pcre_extra* extra;
  int capture_count;
  pcre_jit_stack *jit_stack;
} PCRE_INTERNAL_STRUCT;

void pcre_internal_free(PCRE_INTERNAL_STRUCT *ptr) {
  if (ptr->jit_stack) {
    pcre_jit_stack_free(ptr->jit_stack);
  }
  if (ptr->pcre) {
    pcre_free(ptr->pcre);
  }
  if (ptr->extra) {
#ifdef HAVE_PCRE_FREE_STUDY
    pcre_free_study(ptr->extra);
#else
    pcre_free(ptr->extra);
#endif
  }
}


static VALUE regex_compile(VALUE self, VALUE use_jit)
{
  char *re;
  const char *error_ptr;
  int error_offset;
  int error_code;
  pcre *ptr;
  int do_use_jit = !(use_jit == Qfalse || use_jit == Qnil);

  VALUE pattern = rb_ivar_get(self, ivar_pattern);
  Check_Type(pattern, T_STRING);
  re = StringValuePtr(pattern);

  ptr = pcre_compile2(re, PCRE_UTF8 | PCRE_DUPNAMES, &error_code, &error_ptr, &error_offset, NULL);

  if (ptr) {
    struct PCRE_INTERNAL *pcre_int = ALLOC(struct PCRE_INTERNAL);
    VALUE rb_pcre_int;
    pcre_int->pcre = ptr;
    // Дополнительные сведения + JIT компиляция, если поддерживается
    if (do_use_jit) {
      pcre_int->extra = pcre_study(ptr, PCRE_STUDY_JIT_COMPILE, &error_ptr);
      pcre_int->jit_stack = pcre_jit_stack_alloc(16*1024, 512*1024);
    } else {
      pcre_int->extra = pcre_study(ptr, 0, &error_ptr);
      pcre_int->jit_stack = NULL;
    }
    // Количество переменных захвата
    pcre_fullinfo(pcre_int->pcre, pcre_int->extra, PCRE_INFO_CAPTURECOUNT, &pcre_int->capture_count);

//    if (pcre_int->extra) {
//      printf("JIT!\n");
//    } else {
//      printf("No JIT.\n");
//    }
    rb_pcre_int = Data_Wrap_Struct(cPcreInternal, NULL, pcre_internal_free, pcre_int);
    rb_ivar_set(self, ivar_internal_pcre, rb_pcre_int);
  } else {
    rb_ivar_set(self, ivar_error_text, rb_str_new2(error_ptr));
    rb_ivar_set(self, ivar_error_offset, INT2NUM(error_offset));
    rb_ivar_set(self, ivar_error_code, INT2NUM(error_code));
  }
  return Qnil; // rb_str_new2(re);
}

static VALUE regex_names(VALUE self) {
  VALUE rb_pcre_int = rb_ivar_get(self, ivar_internal_pcre);
  struct PCRE_INTERNAL *pcre_int;
  int name_count;
  Data_Get_Struct(rb_pcre_int, struct PCRE_INTERNAL, pcre_int);


  if (0 == pcre_fullinfo(pcre_int->pcre, pcre_int->extra, PCRE_INFO_NAMECOUNT, &name_count)) {
    int name_entry_size;
    VALUE result;
    const unsigned char *name_entry;
    pcre_fullinfo(pcre_int->pcre, pcre_int->extra, PCRE_INFO_NAMEENTRYSIZE, &name_entry_size);
    pcre_fullinfo(pcre_int->pcre, pcre_int->extra, PCRE_INFO_NAMETABLE, &name_entry);
    result = rb_ary_new();
    for (int i=0; i<name_count; i++) {
      int capture_index = (unsigned int)(name_entry[0]) * 256 + (unsigned int)(name_entry[1]);
      rb_ary_store(result, capture_index-1, rb_str_new2(name_entry+2));
      name_entry += name_entry_size;
    }
    return result;
  } else {
    return Qnil;
  }
}

typedef struct MATCH_DATA {
  PCRE_INTERNAL_STRUCT *pcre_int;
  char *c_str;
  int length;
  int offset;
  int next_position;
  int vector_size;
  int *vector;
} MATCH_DATA_STRUCT;

// Returns Match on qNil
static VALUE regex_do_match(MATCH_DATA_STRUCT *match_data) {
  int offset_count;
  int *vector;

  if (match_data->vector == NULL) {
    // Размер выходного массива (с небольшим запасом)
    match_data->vector_size = 3 + match_data->pcre_int->capture_count * 3;
    match_data->vector = malloc(sizeof(int)*match_data->vector_size);
  }

  if (match_data->pcre_int->extra != NULL && match_data->pcre_int->jit_stack != NULL) {
    offset_count = pcre_jit_exec(match_data->pcre_int->pcre, match_data->pcre_int->extra,
      match_data->c_str, match_data->length,
      match_data->offset, PCRE_NO_UTF8_CHECK, match_data->vector, match_data->vector_size,
      match_data->pcre_int->jit_stack);
  } else {
    offset_count = pcre_exec(match_data->pcre_int->pcre, match_data->pcre_int->extra,
      match_data->c_str, match_data->length,
      match_data->offset, PCRE_NO_UTF8_CHECK, match_data->vector, match_data->vector_size);
  }

  vector = match_data->vector;
  if (offset_count > 0) {
      VALUE rb_vector;
      VALUE match;
      // Create a new object, using the default initializer
      match = rb_class_new_instance(0, NULL, cMatch);
      
      // Позиция следующего элемента равна либо концу захвата, либо следующая, если захваченная строка пуста.
      int len = vector[1]-vector[0];
      match_data->next_position = (len==0) ? vector[0]+1 : vector[1];

      // Позиция начала
      rb_ivar_set(match, ivar_position, INT2NUM(vector[0]));
      // Длина
      rb_ivar_set(match, ivar_length, INT2NUM(len));

      // Значения захваченные передаем в массив
      rb_vector = rb_ary_new2(offset_count);
      for (int i=0, vector_index=0; i<offset_count; i++, vector_index+=2) {
        int start = vector[vector_index];
        int end = vector[vector_index+1];
        if (start != end) {
          VALUE s = rb_utf8_str_new(match_data->c_str+start, end-start);
          rb_ary_store(rb_vector, i, s);
        }
      }
      rb_ivar_set(match, ivar_vector, rb_vector);
      return match;
  } else {
    if (offset_count < PCRE_ERROR_NOMATCH) {
//      printf("Error: %d\n", offset_count);
      rb_raise(rb_eRegexpError, "PCRE exec error: %d", offset_count);
//      rb_ivar_set(match, ivar_error_code, INT2NUM(offset_count));
    }
  }
  return Qnil;
}

void free_match(MATCH_DATA_STRUCT *match_data) {
  if (match_data->vector) { free(match_data->vector); }
}

static VALUE regex_match(VALUE self, VALUE str) {
  MATCH_DATA_STRUCT match_data;
  VALUE rb_pcre_int;
  VALUE match;
  int count;
  
  Check_Type(str, T_STRING);
  match_data.c_str = StringValuePtr(str);
  match_data.length = RSTRING_LEN(str);
  rb_pcre_int = rb_ivar_get(self, ivar_internal_pcre);
  Data_Get_Struct(rb_pcre_int, PCRE_INTERNAL_STRUCT, match_data.pcre_int);
  match_data.offset = 0;
  match_data.vector = NULL;
  // Create a new object, using the default initializer
//  match = rb_class_new_instance(0, NULL, cMatch);
//  match_data.match = &match;

  match = regex_do_match(&match_data);
  free_match(&match_data);
  return match;
}

static VALUE regex_match_all(VALUE self, VALUE str) {
  MATCH_DATA_STRUCT match_data;
  VALUE match;
  VALUE rb_pcre_int;

  Check_Type(str, T_STRING);
  rb_need_block();

  match_data.c_str = rb_string_value_ptr(&str);
  match_data.length = strlen(match_data.c_str);
  rb_pcre_int = rb_ivar_get(self, ivar_internal_pcre);
  Data_Get_Struct(rb_pcre_int, PCRE_INTERNAL_STRUCT, match_data.pcre_int);
  match_data.offset = 0;
  match_data.vector = NULL;

  while(1) {
    VALUE match = regex_do_match(&match_data);
    if (match == Qnil) {
      break;
    }
    rb_yield(match);
    match_data.offset = match_data.next_position;
  }
  free_match(&match_data);
  return Qnil;
}

static VALUE match_arr_get(VALUE self, VALUE index) {
  long idx;
  VALUE vector;

  Check_Type(index, T_FIXNUM);
  idx = NUM2LONG(index);
  vector = rb_ivar_get(self, ivar_vector);
  return rb_ary_entry(vector, idx);
}

void Init_ruby_pcre() {
  sym_puts = rb_intern("puts");

  mPcre = rb_define_module("PCRE");
  cRegex = rb_define_class_under(mPcre, "Regex", rb_cObject);
  rb_define_method(cRegex, "compile", regex_compile, 1);
  rb_remove_method(cRegex, "names");
  rb_define_method(cRegex, "names", regex_names, 0);
  rb_remove_method(cRegex, "match");
  rb_define_method(cRegex, "match", regex_match, 1);
  rb_remove_method(cRegex, "match_all");
  rb_define_method(cRegex, "match_all", regex_match_all, 1);

  cMatch = rb_define_class_under(mPcre, "Match", rb_cObject);
  rb_remove_method(cMatch, "[]");
  rb_define_method(cMatch, "[]", match_arr_get, 1);

  cPcreInternal = rb_define_class_under(mPcre, "PcreInternal", rb_cObject);
  rb_undef_alloc_func(cPcreInternal);

  ivar_error_text = rb_intern("@error_text");
  ivar_error_offset = rb_intern("@error_offset");
  ivar_error_code = rb_intern("@error_code");
  ivar_internal_pcre = rb_intern("@internal_pcre");
  ivar_pattern = rb_intern("@pattern");

  rb_eRegexpError = rb_define_class_under(cRegex, "RegexpError", rb_eStandardError);

  ivar_position = rb_intern("@position");
  ivar_length = rb_intern("@length");
  ivar_vector = rb_intern("@vector");
  ivar_matched_str = rb_intern("@matched_str");

}
