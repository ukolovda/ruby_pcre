module PCRE
  class Match

    attr_reader :position, :length

    def count
      @vector.count
    end

    def [](index)
      # This is stub only for indexing
      # @vector[index]
    end

    def to_s
      @vector ? @vector[0] : ''
    end

    def each
      # This is stub only for indexing
    end
  end
end
