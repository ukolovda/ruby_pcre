module PCRE
  class Regex
    attr_reader :pattern, :error_text, :error_offset, :error_code

    # @param [String] pattern
    def initialize(pattern, jit: true)
      @pattern = pattern
      compile(jit)
    end


    # @return [Array<String>]
    def names
      # This is stub only for indexing
    end

    # @return [PCRE::Match]
    def match(str)
      # This is stub only for indexing
    end

    # @return [PCRE::Match]
    # Will yield for every found match
    def match_all(str)
      # This is stub only for indexing
    end
  end
end
