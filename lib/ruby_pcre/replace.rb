# frozen_string_literal: true

module PCRE
  class Replace

    attr_reader :source

    # @param [String] repl
    def initialize(source)
      @source = source
      compile
    end


    def replace(match)
      case @compiled.length
        when 0
          ''
        when 1
          @compiled[0].get_value(match)
        else
          @compiled.map do |item|
            item.get_value(match)
          end.join
      end
    end

    class Str
      def initialize(char)
        @str = char
      end
      
      def add_char(char)
        @str << char
      end

      def get_value(_source_array)
        @str # regardless of _source_array
      end
    end

    class Num
      def initialize(index)
        @num = index
      end

      def get_value(source_array)
        source_array[@num]
      end
    end

    private

    def compile
      index = 0
      len = @source.length
      @compiled = []
      while index < len
        c = @source[index..index]
        case c
          when '\\'
            index += 1
            add_char @source[index..index]
          when '$'
            num = @source[index+1..-1].to_i
            if num > 0
              @compiled << Num.new(num)
              index += num.to_s.length
            else
              add_char c
            end
          else
            add_char c
        end
        index += 1
      end
    end

    # @param [Char] char
    def add_char(char)
      if @compiled.last&.is_a?(Str)
        @compiled.last.add_char(char)
      else
        @compiled << Str.new(char)
      end
    end
  end
end
