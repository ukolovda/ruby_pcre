# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ruby_pcre/version'

Gem::Specification.new do |spec|
  spec.name          = "ruby_pcre"
  spec.version       = PCRE::VERSION
  spec.authors       = ["ukolovda"]
  spec.email         = ["udmitry@mail.ru"]
  spec.description   = %q{Ruby PCRE bridge}
  spec.summary       = %q{Ruby PCRE bridge}
  spec.homepage      = ""
  spec.license       = "MIT"
  spec.required_ruby_version = '>= 2.5.0'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rake-compiler"
  spec.add_development_dependency "test-unit"
  spec.extensions = %w[ext/ruby_pcre/extconf.rb]
end
