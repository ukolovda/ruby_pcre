require "test/unit"
require "ruby_pcre"

class TestPcre < Test::Unit::TestCase
  def test_create
    pcre = PCRE::Regex.new('Hello!')
    assert_equal 'Hello!', pcre.pattern
    assert_nil pcre.error_text
    assert_nil pcre.error_offset
  end

  def test_create_bad_type
    assert_raises(TypeError) do
      PCRE::Regex.new(Object.new)
    end
  end

  def test_create_with_error
    pcre = PCRE::Regex.new('(hello')
    assert_equal 'missing )', pcre.error_text
    assert_equal 14, pcre.error_code
    assert_equal 6, pcre.error_offset
  end

  def test_should_get_names
    pcre = PCRE::Regex.new('H(e)(?<n1>ll)(?<n2>o), (?<n2>world)!')
    assert_nil pcre.error_code
    assert_equal [nil, 'n1', 'n2', 'n2'], pcre.names
  end

  def test_match_true
    re = PCRE::Regex.new('a(?<n1>bb)(aa)?(?<n2>[cd])')
    match = re.match('n abbc abbd abbe')
    assert_not_nil match
    assert_equal 2, match.position
    assert_equal 4, match.count
    assert_equal 'abbc', match.to_s
    assert_equal 'abbc', match[0]
    assert_equal 'bb', match[1]
    assert_equal nil, match[2]
    assert_equal 'c', match[3]
  end

  def test_match_utf8
    re = PCRE::Regex.new('(кир)')
    match = re.match('кир')
    assert_not_nil match
    assert_equal 'кир', match[0]
    assert_equal 'кир', match[1]
  end

  def test_match_false
    re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])')
    assert_nil re.match('bbbb')
  end

  def test_match_all
    re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])')
    cnt = 0
    re.match_all('abbc abbd abbe') do |match|
      cnt += 1
      assert match.position == (cnt==1 ? 0 : 5)
      assert_equal 4, match.length
      assert match[0] == (cnt==1 ? 'abbc' : 'abbd')
      assert match[1] == 'bb'
      assert match[2] == (cnt==1 ? 'c' : 'd')
    end
    assert_equal 2, cnt
  end

  def test_match_benchmark
    re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])')
    coef=1000000
    s = 'abbc abbd abbe'*coef
    t = Time.now.to_f
    re.match_all(s) do |match|
      match.position
    end
    t = Time.now.to_f - t
    puts "test_match_benchmark: #{t} sec."
  end

  def test_match_benchmark_jit
    re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])', jit: true)
    coef=1000000
    s = 'abbc abbd abbe'*coef
    t = Time.now.to_f
    re.match_all(s) do |match|
      match.position
    end
    t = Time.now.to_f - t
    puts "test_match_benchmark_jit: #{t} sec."
  end

  def test_make_and_release
    t = Time.now.to_f
    (1..100).each do
      re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])')
    end
    t = Time.now.to_f - t
    puts "test_make_and_release: #{t} sec."
  end

  def test_replace
    re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])')
    match = re.match('n abbc abbd abbe')
    replace = PCRE::Replace.new('ab$2f$3')
    # assert_equal ['ab', 2, 'f', 3], replace.instance_variable_get(:@compiled)
    assert_equal 'ab$2f$3', replace.source
    assert_equal 'abcf', replace.replace(match)
  end

  def test_replace_single_item
    re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])')
    match = re.match('n abbc abbd abbe')
    replace = PCRE::Replace.new('$2')
    # assert_equal [2], replace.instance_variable_get(:@compiled)
    assert_equal '$2', replace.source
    assert_equal 'c', replace.replace(match)
  end

  def test_replace_benchmark
    t = Time.now.to_f
    re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])')
    match = re.match('n abbc abbd abbe')
    replace = PCRE::Replace.new('ab$2f')
    (1..100000).each do
      replace.replace(match)
    end
    t = Time.now.to_f - t
    puts "test_replace_benchmark: #{t} sec."
  end

  def test_replace_benchmark_jit
    t = Time.now.to_f
    re = PCRE::Regex.new('a(?<n1>bb)(?<n2>[cd])', jit: true)
    match = re.match('n abbc abbd abbe')
    replace = PCRE::Replace.new('ab$2f')
    (1..100000).each do
      replace.replace(match)
    end
    t = Time.now.to_f - t
    puts "test_replace_benchmark_jit: #{t} sec."
  end

end
